# App de aerolíneas - Ejercicio de UI5

El cliente solicita una aplicación que le permita ver las aerolíneas creadas en el sistema y darlas de alta.

## Backend

- Crear dentro del servicio ZEXERCISE_FLIGHT_SRV dos endpoints: uno que devuelva las aerolíneas de la tabla SCARR y otro que permita crearlas.

## Frontend

- Crear una app que muestre un listado de las compañías existentes junto con sus datos ("Código de aerolinea", "Nombre" y "Moneda") y un formulario que permita generar una nueva aerolínea en el sistema.
- El listado que muestre las aerolíneas, debería permitir filtrar por nombre de la aerolínea. 